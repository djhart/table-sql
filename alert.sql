CREATE TABLE `alert` (
  `alert_id` int(11) NOT NULL DEFAULT '0',
  `app_device_id` bigint(20) NOT NULL DEFAULT '0',
  `app_alert_id` int(11) NOT NULL,
  `app_severity` int(11) NOT NULL DEFAULT '10',
  `edge_severity` int(11) NOT NULL DEFAULT '0',
  `device_id` int(11) DEFAULT NULL,
  `application_id` int(11) NOT NULL DEFAULT '96000001' COMMENT 'Default application_id is 96000001',
  `acknowledged` int(11) DEFAULT '0',
  `acknowledged_id` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `assigned_to_id` int(11) DEFAULT NULL,
  `message` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `origin` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_time_epoch` int(11) DEFAULT NULL,
  `end_time_epoch` int(11) DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `card` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `port` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'archer so can be port number or associated protocol such as UDP/SSH\n',
  `slot` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT 'Holding place - May be extended later to match various types.',
  `customer` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class` int(11) DEFAULT NULL COMMENT 'Another placeholder for future use',
  PRIMARY KEY (`app_device_id`,`app_alert_id`,`application_id`),
  UNIQUE KEY `alert_id_UNIQUE` (`alert_id`),
  KEY `device_id_idx` (`device_id`),
  KEY `alert_id_idx` (`alert_id`),
  CONSTRAINT `fk_alert_to_device` FOREIGN KEY (`device_id`) REFERENCES `device` (`device_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='section:status';
DELIMITER ;;
BEFORE INSERT ON `alert` FOR EACH ROW
begin
set new.end_time = FROM_UNIXTIME(new.end_time_epoch);
set new.start_time = FROM_UNIXTIME(new.start_time_epoch);

end */;;
DELIMITER ;
