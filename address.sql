CREATE TABLE `address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `street_number` int(11) DEFAULT NULL,
  `house_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Some countries use building names or house names instead of numbers',
  `street_number_suffix` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'A, B, Beach, etc',
  `address_type_id` int(11) DEFAULT NULL,
  `address_type_identifier` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Box Number, Apartment Number, floor ',
  `local_municipality` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Could be its own table at some point',
  `governing_district` int(11) DEFAULT NULL COMMENT 'State, province ( Canada ), County ( UK )',
  PRIMARY KEY (`address_id`),
  KEY `fk_address_to_location_idx` (`location_id`),
  CONSTRAINT `fk_address_to_location` FOREIGN KEY (`location_id`) REFERENCES `location` (`location_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='section:location';
