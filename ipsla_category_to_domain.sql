CREATE TABLE `ipsla_category_to_domain` (
  `ipsla_category_id` int(11) NOT NULL DEFAULT '0',
  `domain_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ipsla_category_id`,`domain_id`),
  KEY `fk_ipsla_access_by_category_idx` (`ipsla_category_id`),
  KEY `fk_ipsla_access_by_domain_idx` (`domain_id`),
  CONSTRAINT `fk_category_to_domain` FOREIGN KEY (`domain_id`) REFERENCES `domain` (`domain_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_domain_to_category` FOREIGN KEY (`ipsla_category_id`) REFERENCES `ipsla_category` (`ipsla_category_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
