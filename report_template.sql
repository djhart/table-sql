CREATE TABLE `report_template` (
  `report_template_id` int(11) NOT NULL,
  `name` varchar(256) DEFAULT NULL,
  `is_custom` int(11) NOT NULL DEFAULT '1',
  `app_id` int(11) NOT NULL,
  PRIMARY KEY (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='section:reporting';
