CREATE TABLE `task_manager` (
  `task_manager_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `time_queued` int(11) DEFAULT NULL,
  `last_completed` int(11) DEFAULT NULL,
  `run_time` int(11) DEFAULT NULL,
  `objects_updated` int(11) DEFAULT NULL,
  PRIMARY KEY (`task_id`),
  CONSTRAINT `fk_task_manager_to_task` FOREIGN KEY (`task_id`) REFERENCES `task` (`task_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='section:management';
