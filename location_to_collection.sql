CREATE TABLE `location_to_collection` (
  `location_id` int(11) NOT NULL,
  `collection_id` int(11) NOT NULL,
  PRIMARY KEY (`location_id`,`collection_id`),
  KEY `fk_collection_to_location_idx` (`location_id`),
  KEY `fk_location_to_collection_idx` (`collection_id`),
  CONSTRAINT `fk_collection_to_location` FOREIGN KEY (`location_id`) REFERENCES `location` (`location_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_location_to_collection` FOREIGN KEY (`collection_id`) REFERENCES `collection` (`collection_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
