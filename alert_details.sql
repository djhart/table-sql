SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
 1 AS `vendor`,
 1 AS `application_name`,
 1 AS `device_name`,
 1 AS `ip_address`,
 1 AS `count`,
 1 AS `origin`,
 1 AS `app_severity`,
 1 AS `edge_severity`,
 1 AS `message`,
 1 AS `alert_id`,
 1 AS `app_device_id`,
 1 AS `app_alert_id`,
 1 AS `device_id`,
 1 AS `application_id`,
 1 AS `acknowledged_id`,
 1 AS `assigned_to_id`,
 1 AS `end_time`,
 1 AS `start_time`,
 1 AS `dns_name`,
 1 AS `start_time_epoch`,
 1 AS `end_time_epoch`*/;
SET character_set_client = @saved_cs_client;
