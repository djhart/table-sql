CREATE TABLE `summary` (
  `summary_id` int(11) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `next_available_id` int(11) DEFAULT '1',
  `total_entries` int(11) DEFAULT NULL,
  `number_default` int(11) DEFAULT NULL COMMENT 'Number of default entries ( above 50 Million )',
  `number_custom` int(11) DEFAULT NULL COMMENT 'Number of custom entries ( below 50 Million )',
  PRIMARY KEY (`summary_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='section:management';
