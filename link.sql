CREATE TABLE `link` (
  `link_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL COMMENT 'This will assume that links are for city to city or location to location rather than a more complex device to device or interface to interface',
  `destination_id` int(11) NOT NULL,
  `severity` int(11) NOT NULL DEFAULT '0',
  `message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`source_id`,`destination_id`),
  KEY `fk_link_to_dLocation_idx` (`destination_id`),
  KEY `fk_link_to_sLocation_idx` (`source_id`),
  KEY `link_id_idx` (`link_id`),
  CONSTRAINT `fk_link_to_dLocation` FOREIGN KEY (`destination_id`) REFERENCES `location` (`location_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_link_to_sLocation` FOREIGN KEY (`source_id`) REFERENCES `location` (`location_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='section:link';
