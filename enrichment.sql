CREATE TABLE `enrichment` (
  `enrichment_id` int(11) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enrichment_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`enrichment_id`),
  KEY `fk_enrichment_to_application_idx` (`application_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='section:enrichment';
