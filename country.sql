CREATE TABLE `country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sovereign_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alpha_2_code` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alpha_3_code` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`country_id`),
  KEY `fk_country_to_location_id_idx` (`location_id`),
  CONSTRAINT `fk_country_to_location_id` FOREIGN KEY (`location_id`) REFERENCES `location` (`location_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=91000222 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='section:location';
