CREATE TABLE `location_to_domain` (
  `location_id` int(11) NOT NULL,
  `domain_id` int(11) NOT NULL,
  PRIMARY KEY (`location_id`,`domain_id`),
  KEY `fk_location_to_domain_id_idx` (`location_id`),
  KEY `fk_location_access_to_domain_id_idx` (`domain_id`),
  CONSTRAINT `fk_domain_access_to_location_id` FOREIGN KEY (`location_id`) REFERENCES `location` (`location_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_location_to_domain` FOREIGN KEY (`domain_id`) REFERENCES `domain` (`domain_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
