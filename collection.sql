CREATE TABLE `collection` (
  `collection_id` int(11) NOT NULL DEFAULT '0' COMMENT 'This is the id used by Edge',
  `name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_collection_id` int(11) NOT NULL DEFAULT '0' COMMENT 'This the the id from the originating application.  For instance, if this is a a User Group with an id of 10, this field will contain 10.',
  `parent_id` int(11) DEFAULT '0' COMMENT 'This field contains the id of the parent, if one exists.  This field points to the app_collection_id of the parent.',
  `application_id` int(11) NOT NULL DEFAULT '0',
  `collection_type_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`app_collection_id`,`collection_type_id`,`application_id`),
  KEY `parentId` (`parent_id`),
  KEY `fk_collection_to_application_idx` (`application_id`),
  KEY `fk_collection_to_collection_type_idx` (`collection_type_id`),
  KEY `collection_id_idx` (`collection_id`),
  CONSTRAINT `fk_collection_to_application` FOREIGN KEY (`application_id`) REFERENCES `application` (`application_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_collection_to_collection_type` FOREIGN KEY (`collection_type_id`) REFERENCES `collection_type` (`collection_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='section:collection';
