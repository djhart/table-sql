CREATE TABLE `location` (
  `location_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `severity` int(11) DEFAULT '0',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` double NOT NULL DEFAULT '0',
  `latitude` double NOT NULL DEFAULT '0',
  `location_type_id` int(11) NOT NULL DEFAULT '0',
  `population` int(11) NOT NULL DEFAULT '0',
  `timezone_id` int(11) NOT NULL DEFAULT '0',
  `capital_type_id` int(11) DEFAULT '0',
  `elevation_m` int(11) NOT NULL DEFAULT '0',
  `elevation_ft` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`longitude`,`latitude`,`location_type_id`),
  KEY `fk_location_type_id` (`location_type_id`),
  KEY `fk_timezone_id_idx` (`timezone_id`),
  KEY `fk_capital_type_id_idx` (`capital_type_id`),
  KEY `fk_location_id_idx` (`location_id`),
  CONSTRAINT `fk_capital_type_id` FOREIGN KEY (`capital_type_id`) REFERENCES `capital_type` (`capital_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_location_type` FOREIGN KEY (`location_type_id`) REFERENCES `location_type` (`location_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_timezone_id` FOREIGN KEY (`timezone_id`) REFERENCES `timezone` (`timezone_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='section:location';
DELIMITER ;;
FOR EACH ROW
begin
set @new_count = (select coalesce(max(l.location_id),0) + 1 from location l where location_id < 50000000);
update summary s set s.next_available_id = (@new_count) where s.table_name = "location";
end */;;
DELIMITER ;
