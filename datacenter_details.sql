SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
 1 AS `datacenter_id`,
 1 AS `name`,
 1 AS `latitude`,
 1 AS `longitude`,
 1 AS `severity`,
 1 AS `description`,
 1 AS `collection_id`,
 1 AS `app_collection_id`,
 1 AS `parent_id`,
 1 AS `application`,
 1 AS `vendor`*/;
SET character_set_client = @saved_cs_client;
