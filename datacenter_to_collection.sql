CREATE TABLE `datacenter_to_collection` (
  `datacenter_id` int(11) NOT NULL,
  `collection_id` int(11) NOT NULL,
  PRIMARY KEY (`datacenter_id`,`collection_id`),
  KEY `fk_datacenter_to_collection_idx` (`collection_id`),
  CONSTRAINT `fk_collection_to_datacenter` FOREIGN KEY (`datacenter_id`) REFERENCES `datacenter` (`datacenter_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_datacenter_to_collection` FOREIGN KEY (`collection_id`) REFERENCES `collection` (`app_collection_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
