CREATE TABLE `task` (
  `task_id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `frequency` int(11) NOT NULL DEFAULT '300' COMMENT 'In seconds',
  `depends_on` int(11) NOT NULL DEFAULT '0' COMMENT 'task_id that this depends on',
  `adapter_id` int(11) NOT NULL,
  `enabled` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`adapter_id`,`name`),
  UNIQUE KEY `task_id_UNIQUE` (`task_id`),
  CONSTRAINT `fk_task_to_adapter` FOREIGN KEY (`adapter_id`) REFERENCES `adapter` (`adapter_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='section:management';
