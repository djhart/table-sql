CREATE TABLE `application_config` (
  `application_config_id` int(11) NOT NULL,
  `hostname` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `api_type_id` int(11) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  KEY `fk_app_config_to_app_idx` (`application_id`),
  KEY `fk_app_to_api_type_idx` (`api_type_id`),
  CONSTRAINT `fk_app_config_to_app` FOREIGN KEY (`application_id`) REFERENCES `application` (`application_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_app_to_api_type` FOREIGN KEY (`api_type_id`) REFERENCES `api_type` (`api_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='section:application';
