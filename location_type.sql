CREATE TABLE `location_type` (
  `location_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`location_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=97000005 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='section:location';
