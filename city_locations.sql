SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
 1 AS `country_name`,
 1 AS `governing_district`,
 1 AS `city`,
 1 AS `latitude`,
 1 AS `longitude`,
 1 AS `severity`,
 1 AS `city_id`,
 1 AS `location_id`*/;
SET character_set_client = @saved_cs_client;
