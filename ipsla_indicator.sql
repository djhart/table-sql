CREATE TABLE `ipsla_indicator` (
  `indicator_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ipsla_category_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`indicator_id`),
  KEY `fk_ipsla_category_idx` (`ipsla_category_id`),
  KEY `name_idx` (`name`,`indicator_id`),
  KEY `name_only_idx` (`name`),
  CONSTRAINT `fk_indicator_to_category` FOREIGN KEY (`ipsla_category_id`) REFERENCES `ipsla_category` (`ipsla_category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=80000108 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='section:link';
