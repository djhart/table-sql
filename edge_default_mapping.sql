CREATE TABLE `edge_default_mapping` (
  `edge_default_mapping_id` int(11) NOT NULL AUTO_INCREMENT,
  `table` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'What is this used for?  default cities, countries, vendor mappings...',
  `starting_value` int(11) DEFAULT NULL COMMENT 'Starting value for this table',
  `starting_value_readable` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Text to make starting value easy to read',
  `purpose` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'What kind of information is populated into the table',
  PRIMARY KEY (`edge_default_mapping_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='section:management';
