SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
 1 AS `location_name`,
 1 AS `latitude`,
 1 AS `longitude`,
 1 AS `severity`,
 1 AS `country_id`,
 1 AS `location_id`,
 1 AS `name`,
 1 AS `alpha_2_code`,
 1 AS `alpha_3_code`*/;
SET character_set_client = @saved_cs_client;
