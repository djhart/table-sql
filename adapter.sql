CREATE TABLE `adapter` (
  `adapter_id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` int(11) NOT NULL DEFAULT '0',
  `application_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`adapter_id`),
  KEY `fk_adapter_to_application_idx` (`application_id`),
  CONSTRAINT `fk_adapter_to_application` FOREIGN KEY (`application_id`) REFERENCES `application` (`application_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='section:application';
