CREATE TABLE `status` (
  `table_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Table that has the entity: device, alert, collection',
  `table_entity_id` int(11) NOT NULL COMMENT 'This is the id from the table.\n\nFor table:device\n	id:device_id\n\n',
  `status_type_id` int(11) NOT NULL,
  `scope_id` int(11) NOT NULL,
  `severity` int(11) NOT NULL,
  PRIMARY KEY (`table_name`,`table_entity_id`,`status_type_id`,`scope_id`),
  KEY `fk_status_to_scope_idx` (`scope_id`),
  KEY `fk_status_to_type_idx` (`status_type_id`),
  CONSTRAINT `fk_status_to_scope` FOREIGN KEY (`scope_id`) REFERENCES `scope` (`scope_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_status_to_type` FOREIGN KEY (`status_type_id`) REFERENCES `status_type` (`status_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='section:status';
