CREATE TABLE `protocol` (
  `protocol_id` int(11) NOT NULL,
  `decimal` int(11) DEFAULT NULL,
  `hex` varchar(28) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `references` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `protocol_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`protocol_id`),
  KEY `fk_protocol_to_type_idx` (`protocol_type_id`),
  CONSTRAINT `fk_protocol_to_type` FOREIGN KEY (`protocol_type_id`) REFERENCES `protocol_type` (`protocol_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='section:link';
