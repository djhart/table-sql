CREATE TABLE `report_attachment` (
  `report_attachment_id` int(11) NOT NULL,
  `report_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`report_attachment_id`),
  KEY `fk_attachment_to_report_idx` (`report_id`),
  CONSTRAINT `fk_attachment_to_report` FOREIGN KEY (`report_id`) REFERENCES `report` (`report_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='section:reporting';
