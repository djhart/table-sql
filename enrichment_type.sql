CREATE TABLE `enrichment_type` (
  `enrichment_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`enrichment_type_id`),
  CONSTRAINT `fk_enrichment_type_to_enrichment` FOREIGN KEY (`enrichment_type_id`) REFERENCES `enrichment` (`enrichment_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='section:enrichment';
