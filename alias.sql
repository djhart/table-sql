CREATE TABLE `alias` (
  `table_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `table_id` int(11) NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`table_name`,`table_id`,`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='section:location';
