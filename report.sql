CREATE TABLE `report` (
  `report_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_private` int(11) DEFAULT NULL,
  `frequency_unit` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `next_run` int(11) DEFAULT NULL,
  `timezone` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`report_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='section:reporting';
