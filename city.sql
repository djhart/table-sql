CREATE TABLE `city` (
  `city_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `governing_district_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`city_id`),
  KEY `fk_city_to_location_id_idx` (`location_id`),
  KEY `fk_city_to_governing_district_id_idx` (`governing_district_id`),
  KEY `name_idx` (`name`,`city_id`) USING BTREE,
  CONSTRAINT `fk_city_to_governing_district_id` FOREIGN KEY (`governing_district_id`) REFERENCES `governing_district` (`governing_district_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_to_location_id` FOREIGN KEY (`location_id`) REFERENCES `location` (`location_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='section:location';
