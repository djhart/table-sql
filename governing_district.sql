CREATE TABLE `governing_district` (
  `governing_district_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `governing_district_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`governing_district_id`),
  KEY `fk_governing_district_to_country_idx` (`country_id`),
  KEY `fk_governing_district_to_location_idx` (`location_id`),
  KEY `fk_governing_district_to_type_idx` (`governing_district_type_id`),
  CONSTRAINT `fk_governing_district_to_country` FOREIGN KEY (`country_id`) REFERENCES `country` (`country_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_governing_district_to_location` FOREIGN KEY (`location_id`) REFERENCES `location` (`location_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_governing_district_to_type` FOREIGN KEY (`governing_district_type_id`) REFERENCES `governing_district_type` (`governing_district_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=92002591 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='section:location';
