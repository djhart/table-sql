SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
 1 AS `device_id`,
 1 AS `device_name`,
 1 AS `app_device_id`,
 1 AS `device_type_id`,
 1 AS `device_type`,
 1 AS `dns_name`,
 1 AS `alias`,
 1 AS `ip_address`,
 1 AS `ip_address_int`,
 1 AS `severity`,
 1 AS `description`,
 1 AS `application_id`,
 1 AS `application`,
 1 AS `status_severity`,
 1 AS `scope`,
 1 AS `status_type`,
 1 AS `table_name`*/;
SET character_set_client = @saved_cs_client;
