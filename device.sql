CREATE TABLE `device` (
  `device_id` int(11) NOT NULL COMMENT 'id used by Edge',
  `app_device_id` bigint(20) NOT NULL DEFAULT '0' COMMENT 'id for this device by the originating application\n\nHas to be BIGINT because we’re storing Netcool Omnibus int value of ip address as its app_device_id ',
  `application_id` int(11) NOT NULL DEFAULT '0',
  `dns_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_address_int` bigint(20) DEFAULT NULL,
  `severity` int(11) DEFAULT '0' COMMENT 'This field is a legacy field and will probably be removed.  Please see the section on ‘Status’',
  `description` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `device_type_id` int(11) NOT NULL DEFAULT '86000000',
  PRIMARY KEY (`application_id`,`app_device_id`,`device_type_id`),
  UNIQUE KEY `device_id_UNIQUE` (`device_id`),
  KEY `fk_device_to_device_type_idx` (`device_type_id`),
  KEY `device_id_idx` (`device_id`),
  CONSTRAINT `fk_device_to_type` FOREIGN KEY (`device_type_id`) REFERENCES `device_type` (`device_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='section:device';
DELIMITER ;;
begin
  set NEW.ip_address_int = INET_ATON(NEW.ip_address);
end */;;
DELIMITER ;
